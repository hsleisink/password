#!/usr/bin/php
<?php
	/* Before usage of this script, install this new password manager and create
	 * users with the same password as they have in the current password manager.
	 */
	define("LOCATION_OLD_PM", "/path/to/your/old/password/manager");

	if (($config_old = @file(LOCATION_OLD_PM."/settings/banshee.conf")) === false) {
		exit("Previous version of the Password Manager was not found. Edit this script and set LOCATION_OLD_PM.\n");
	}

	chdir(__DIR__);
	require("../libraries/core/banshee.php");
	require("../libraries/core/security.php");
	require("../libraries/protocols/aes256.php");
	require("../libraries/protocols/rsa.php");
	require(LOCATION_OLD_PM."/libraries/aes256.php");

	error_reporting(E_ALL & ~E_NOTICE);

	$db_new = new Banshee\Database\MySQLi_connection(DB_HOSTNAME, DB_DATABASE, DB_USERNAME, DB_PASSWORD);
	if ($db_new->connected == false) {
		exit("Internal error: database not available.\n");
	}

	foreach ($config_old as $line) {
		list($key, $value) = explode(" = ", trim($line));
		if ($key == "DB_HOSTNAME") {
			$hostname = $value;
		} else if ($key == "DB_DATABASE") {
			$database = $value;
		} else if ($key == "DB_USERNAME") {
			$username = $value;
		} else if ($key == "DB_PASSWORD") {
			$password = $value;
		}
	}

	$db_old = new Banshee\Database\MySQLi_connection($hostname, $database, $username, $password);
	if ($db_old->connected == false) {
		exit("Internal error: database (current) not available.\n");
	}
	$db_old->make_read_only();

	if (count($argv) <= 1) {
		printf("Usage: %s <username>\n", $argv[0]);
		exit;
	}

	do {
		print "Enter password: ";
		system("/bin/stty -echo");
		$password = trim(fgets(STDIN));
		system("/bin/stty echo");
		print "\n";
	} while ($password == "");

	class migrate {
		private $db_old = null;
		private $db_new = null;
		private $username = null;
		private $password = null;
		private $user_old = null;
		private $user_new = null;
		private $container_translate = array();

		public function __construct ($db_old, $db_new, $username, $password) {
			$this->db_old = $db_old;
			$this->db_new = $db_new;
			$this->username = $username;
			$this->password = $password;
		}

		private function migrate_passwords($container_id) {
			$query = "select * from passwords where container_id=%d order by name";
			if (($passwords = $this->db_old->execute($query, $container_id)) === false) {
				return false;
			}

			$aes = new AES256($this->password);
			$crypto_key = $aes->decrypt($this->user_old["crypto_key"]);
			unset($aes);

			$aes_old = new AES256($crypto_key);

			foreach ($passwords as $password) {
				$password["id"] = null;
				$password["container_id"] = $this->container_translate[$container_id];
				$password["crypto_key"] = random_string(32);
				$password["password"] = $aes_old->decrypt($password["password"]);
				$password["info"] = $aes_old->decrypt($password["info"]);

				$aes = new \Banshee\Protocols\AES256($password["crypto_key"]);
				$password["password"] = $aes->encrypt($password["password"]);
				$password["info"] = $aes->encrypt($password["info"]);

				$rsa = new \Banshee\Protocols\RSA($this->user_new["public_key"]);
				if (($password["crypto_key"] = $rsa->encrypt_with_public_key($password["crypto_key"])) == false) {
					return false;
				}

				if ($this->db_new->insert("passwords", $password) === false) {
					return false;
				}
			}

			return true;
		}

		private function migrate_containers(&$containers, $parent_id) {
			foreach ($containers as $i => $container) {
				if ($container["parent_id"] != $parent_id) {
					continue;
				}

				$id = (int)$container["id"];
				$container["id"] = null;
				$container["user_id"] = (int)$this->user_new["id"];

				if ($container["parent_id"] != null) {
					if (($parent = $this->container_translate[$container["parent_id"]]) == null) {
						print "Container parent not found.\n";
						return false;
					}
					$container["parent_id"] = $parent;
				}

				if ($this->db_new->insert("containers", $container) === false) {
					print "Error creating container.\n";
					return false;
				}

				unset($containers[$i]);

				$this->container_translate[$id] = $this->db_new->last_insert_id;

				if ($this->migrate_passwords($id) == false) {
					return false;
				}

				if ($this->migrate_containers($containers, $id) == false) {
					return false;
				}
			}

			return true;
		}

		public function execute() {
			if (($this->user_old = $this->db_old->entry("users", $this->username, "username")) == false) {
				printf("User '%s' not found in old database.\n", $this->username);
				return;
			}

			if (hash_pbkdf2("sha256", $this->password, hash("sha256", $this->username), 100000, 0) != $this->user_old["password"]) {
				printf("Incorrect password.\n");
				return;
			}

			if (($this->user_new = $this->db_new->entry("users", $this->username, "username")) == false) {
				printf("User '%s' not found in new database.\n", $this->username);
				return;
			}

			$this->db_new->query("begin");

			$this->db_new->query("delete from shared where user_id=%d", $this->user_new["id"]);
			$this->db_new->query("delete from shared where password_id in ".
			                     "(select id from passwords where container_id in ".
			                     "(select id from containers where user_id=%d))", $this->user_new["id"]);
			$this->db_new->query("delete from passwords where container_id in ".
			                     "(select id from containers where user_id=%d)", $this->user_new["id"]);
			$this->db_new->query("update containers set parent_id=null where user_id=%d", $this->user_new["id"]);
			$this->db_new->query("delete from containers where user_id=%d", $this->user_new["id"]);

			$query = "select * from containers where user_id=%d order by name";
			if (($containers = $this->db_old->execute($query, $this->user_old["id"])) === false) {
				$this->db_new->query("rollback");
				exit("Error fetching containers\n");
			}

			if ($this->migrate_containers($containers, null) == false) {
				$this->db_new->query("rollback");
				print "Error creating containers\n";
				return false;
			}

			$this->db_new->query("commit");
			print "All ok\n";
		}
	}

	$migrate = new migrate($db_old, $db_new, $argv[1], $password);
	$migrate->execute();
?>
