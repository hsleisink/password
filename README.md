Password Manager
================

The Password Manager is a web based tool to securely store credentials. The application uses AES-256-GCM and RSA-OAEP 4096 bits to protect the credentials. Credentials can be shared with other users via secure internal key exchange.
