<?php
	class shared_controller extends Banshee\controller {
		protected $prevent_repost = false;

		private function show_overview() {
			if (($passwords = $this->model->get_passwords()) === false) {
				return;
			}

			$owner = null;

			$this->view->open_tag("overview");
			foreach ($passwords as $password) {
				if ($password["fullname"] != $owner) {
					$owner = $password["fullname"];
				} else {
					unset($password["fullname"]);
				}
				$this->view->record($password, "password");
			}
			$this->view->close_tag();
		}

		private function show_password($password) {
			$this->view->add_javascript("shared.js");
			$this->view->run_javascript("load_values(".$password["id"].")");

			unset($password["info"]);
			$this->view->record($password, "password");
		}

		private function ajax_values($password_id) {
			if (valid_input($password_id, VALIDATE_NUMBERS, VALIDATE_NONEMPTY) == false) {
				$this->view->add_tag("error", "Invalid password id.");
				return;
			}

			if (($values = $this->model->get_password($password_id)) == false) {
				$this->view->add_tag("error", "Unknown password.");
				return;
			}

			if (($values["password"] === false) || ($values["info"] === false)) {
				$this->view->add_tag("error", "Error decrypting password.");
			} else {
				$this->view->add_tag("error", "none");
				$this->view->add_tag("password", $values["password"]);
				$this->view->add_tag("info", $values["info"]);
			}
		}

		public function execute() {
			if ($this->page->ajax_request) {
				if ($this->page->parameter_numeric(0)) {
					$this->ajax_values($this->page->parameters[0]);
				} else {
					$this->view->add_tag("error", "Password id missing.");
				}
				return;
			}

			if ($this->page->parameter_numeric(0)) {
				if (($password = $this->model->get_password($this->page->parameters[0])) == false) {
					$this->view->add_tag("result", "Password not found.");
				} else {
					$this->show_password($password);
				}
			} else {
				$this->show_overview();
			}
		}
	}
?>
