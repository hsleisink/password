<?php
	class password_controller extends Banshee\controller {
		private $url = array("url" => "container");

		private function show_crumbs($container_id) {
			$this->view->open_tag("crumbs");

			if (($crumbs = $this->model->get_crumbs($container_id)) !== false) {
				foreach ($crumbs as $crumb) {
					$this->view->add_tag("crumb", $crumb["name"], array("id" => $crumb["id"]));
				}
			}

			$this->view->close_tag();
		}

		private function show_password($password_id) {
			if (($password = $this->model->get_password($password_id)) === false) {
				$this->view->add_tag("result", "Password not found.", $this->url);
				return;
			}

			$this->view->add_javascript("password.js");
			$this->view->run_javascript("load_values(".$password_id.")");

			unset($password["crypto_key"]);
			unset($password["info"]);
			$this->view->record($password, "password");

			$this->show_crumbs($password["container_id"]);
		}

		private function show_password_form($password) {
			$this->view->add_javascript("password.js");

			$this->view->open_tag("edit");

			unset($password["crypto_key"]);
			$this->view->record($password, "password");

			if (isset($password["id"])) {
				if (($containers = $this->model->get_all_containers()) !== false) {
					$this->view->open_tag("containers");
					foreach ($containers as $cont) {
						$this->view->add_tag("container", $cont["name"], array("id" => $cont["id"]));
					}
					$this->view->close_tag();
				}
			}

			$this->view->close_tag();

			$this->show_crumbs($password["container_id"]);
		}

		private function show_share_form($password_id) {
			if (($password = $this->model->get_password($password_id)) === false) {
				$this->view->add_tag("result", "Password not found.\n");
				return;
			}

			if (($users = $this->model->get_users()) == false) {
				$this->view->add_tag("result", "No other users to share with.", array("url" => "password/".$password_id));
				return;
			}

			if (($shares = $this->model->get_shares($password["id"])) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			$this->view->open_tag("share");

			unset($password["crypto_key"]);
			$this->view->record($password, "password");

			$access = array("No access", "Read only");
			$this->view->open_tag("access");
			foreach ($access as $level => $label) {
				$this->view->add_tag("level", $label, array("value" => $level));
			}
			$this->view->close_tag();

			$this->view->open_tag("users");
			foreach ($users as $user) {
				if (($share = ($shares[$user["id"]] ?? null)) != null) {
					$user["access"] = $share["access"];
				} else {
					$user["access"] = SHARE_ACCESS_NONE;
				}
				$this->view->record($user, "user");
			}
			$this->view->close_tag();

			$this->view->close_tag();
		}

		private function ajax_values($password_id) {
			if (valid_input($password_id, VALIDATE_NUMBERS, VALIDATE_NONEMPTY) == false) {
				$this->view->add_tag("error", "Invalid password id.");
				return;
			}

			if (($values = $this->model->get_password($password_id)) == false) {
				$this->view->add_tag("error", "Unknown password.");
				return;
			}

			if (($values["password"] === false) || ($values["info"] === false)) {
				$this->view->add_tag("error", "Error decrypting password.");
			} else {
				$this->view->add_tag("error", "none");
				$this->view->add_tag("password", $values["password"]);
				$this->view->add_tag("info", $values["info"]);
			}
		}

		private function random_password() {
			$this->view->add_tag("password", random_string(20));
		}

		public function execute() {
			if ($this->page->ajax_request) {	
				switch ($this->page->parameters[0]) {
					case "get":
						$this->ajax_values($this->page->parameters[1]);
						break;
					case "random":
						$this->random_password();
						break;
				}
				return;
			}

			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				$this->url["url"] .= "/".($_POST["container_id"] ?? "");

				if ($_POST["submit_button"] == "Save password") {
					/* Save password
					 */
					if ($this->model->save_oke($_POST) == false) {
						$this->show_password_form($_POST);
					} else if (isset($_POST["id"]) === false) {
						/* Create password
						 */
						if ($this->model->create_password($_POST) === false) {
							$this->view->add_message("Error creating password.");
							$this->show_password_form($_POST);
						} else {
							$this->user->log_action("password created");
							$this->show_password($this->db->last_insert_id);
						}
					} else {
						/* Update password
						 */
						if ($this->model->update_password($_POST) === false) {
							$this->view->add_message("Error updating password.");
							$this->show_password_form($_POST);
						} else {
							$this->user->log_action("password updated");
							$this->show_password($_POST["id"]);
						}
					}
				} else if ($_POST["submit_button"] == "Delete password") {
					/* Delete password
					 */
					if ($this->model->delete_oke($_POST) == false) {
						$this->show_password_form($_POST);
					} else if ($this->model->delete_password($_POST["id"]) === false) {
						$this->view->add_message("Error deleting password.");
						$this->show_password_form($_POST);
					} else {
						$this->user->log_action("password deleted");
						$this->view->add_tag("result", "Password deleted.", $this->url);
						header("Location: /".$this->url["url"]);
					}
				} else if ($_POST["submit_button"] == "Save sharing") {
					/* Save sharing
					 */
					if ($this->model->save_share($_POST) == false) {
						$this->view->add_message("Error deleting password.");
						$this->show_share_form($_POST["password_id"]);
					} else {
						$this->user->log_action("password share changed");
						$this->show_password($_POST["password_id"]);
					}
				} else {
					$this->view->add_tag("result", "Password not found.", $this->url);
				}
			} else if ($this->page->parameter_numeric(0)) {
				if ($this->page->parameter_value(1, "new")) {
					/* New password
					 */
					if ($this->model->get_container($this->page->parameters[0]) == false) {
						$this->view->add_tag("result", "Parent container not found.", array("url" => "container"));
					} else {
						$password = array("container_id" => $this->page->parameters[0]);
						$this->show_password_form($password);
					}
				} else if ($this->page->parameter_value(1, "edit")) {
					/* Edit password
					 */
					if (($password = $this->model->get_password($this->page->parameters[0])) === false) {
						$this->view->add_tag("result", "Password not found.\n");
					} else {
						$this->show_password_form($password);
					}
				} else if ($this->page->parameter_value(1, "share")) {
					/* Share password
					 */
					$this->show_share_form($this->page->parameters[0]);
				} else {
					$this->show_password($this->page->parameters[0]);
				}
			} else {
				/* Show error
				 */
				$this->view->add_tag("result", "No password selected.", $this->url);
			}
		}
	}
?>
