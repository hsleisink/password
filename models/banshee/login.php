<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * This file is part of the Banshee PHP framework
	 * https://www.banshee-php.org/
	 *
	 * Licensed under The MIT License
	 */

	/* Because the model file is loaded before any output is generated,
	 * it is used to handle the login submit.
	 */

	/* Prevent brute-force attacks
	 */
	if (($lh = $_settings->login_history) == null) {
		$lh = array();
	}
	foreach ($lh as $ip => $record) {
		if ($record["timestamp"] < time() - (LOGIN_BAN_MINUTES * 60)) {
			unset($lh[$ip]);
		}
	}
	if (isset($lh[$_SERVER["REMOTE_ADDR"]]) == false) {
		$lh[$_SERVER["REMOTE_ADDR"]] = array(
			"attempts"  => 0,
			"timestamp" => time());
	}
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		$lh[$_SERVER["REMOTE_ADDR"]]["attempts"]++;
	}

	if ($lh[$_SERVER["REMOTE_ADDR"]]["attempts"] > LOGIN_ALLOWED_FAILS) {
		header("X-Hiawatha-Ban: ".(LOGIN_BAN_MINUTES * 60), true);
		return;
	}

	/* Check login
	 */
	$login_successful = false;
	if (($_SERVER["REQUEST_METHOD"] == "POST") && (($_POST["submit_button"] ?? null) == "Login")) {
		/* Login via password
		 */
		if ($_user->login_password($_POST["username"], $_POST["password"], $_POST["code"] ?? null)) {
			if (is_true($_POST["bind_ip"])) {
				$_session->bind_to_ip();
			}

			$post_protection = new \Banshee\POST_protection($_page, $_user, $_view);
			if (isset($_POST["postdata"]) == false) {
				$post_protection->register_post();

				$_SERVER["REQUEST_METHOD"] = "GET";
				$_POST = array();
			} else if (is_true($_POST["repost"])) {
				$token = $_POST[$post_protection->csrf_key];
				$_POST = json_decode(base64_decode($_POST["postdata"]), true);
				$_POST[$post_protection->csrf_key] = $token;
			}

			$login_successful = true;
		} else {
			if (valid_input($_POST["username"], VALIDATE_LETTERS, VALIDATE_NONEMPTY)) {
				$_user->log_action("login failed for username %s", $_POST["username"]);
			} else {
				$_user->log_action("login failed, possibly the password was entered as the username");
			}

			if ($_settings->notify_prowl_key != null) {
				$prowl = new \Banshee\Protocols\Prowl("Password Manager", $_settings->notify_prowl_key);
				$prowl->send_notification("Invalid login", "From ".$_SERVER["REMOTE_ADDR"]);
			}
		}
	}

	/* Pre-login actions
	 */
	if ($login_successful) {
		/* Load requested page
		 */
		if (($next_page = ltrim($_page->url, "/")) == "") {
			$next_page = $_settings->start_page;
		}

		$_page->select_module($next_page);
		$_view->set_layout();
		if ($_page->module != LOGIN_MODULE) {
			if (file_exists($file = "../models/".$_page->module.".php")) {
				include($file);
			}
		}

		unset($lh[$_SERVER["REMOTE_ADDR"]]);
	} else if ($lh[$_SERVER["REMOTE_ADDR"]]["attempts"] == (LOGIN_ALLOWED_FAILS + 1)) {
		$_user->log_action("IP %s banned after failed logins for username %s", $_SERVER["REMOTE_ADDR"], $_POST["username"]);
	}

	$_settings->login_history = $lh;
?>
