<?php
	class shared_model extends Banshee\model {
		public function get_passwords() {
			$query = "select p.id, p.name, p.url, u.fullname ".
			         "from shared s, passwords p, containers c, users u ".
			         "where s.password_id=p.id and p.container_id=c.id and c.user_id=u.id and s.user_id=%d ".
			         "order by u.fullname, p.name";

			return $this->db->execute($query, $this->user->id);
		}

		public function get_password($password_id) {
			$query = "select p.id, s.crypto_key, p.name, p.url, p.username, p.password, p.info ".
			         "from passwords p, shared s where p.id=s.password_id and p.id=%d and s.user_id=%d";
			if (($result = $this->db->execute($query, $password_id, $this->user->id)) == false) {
				return false;
			}
			$password = $result[0];

			$rsa = new \Banshee\Protocols\RSA($_COOKIE["private_key"].$_SESSION["private_key"]);
			$password["crypto_key"] = $rsa->decrypt_with_private_key($password["crypto_key"]);

			$aes = new Banshee\Protocols\AES256($password["crypto_key"]);
			$password["password"] = $aes->decrypt($password["password"]);
			$password["info"] = $aes->decrypt($password["info"]);

			unset($password["crypto_key"]);

			return $password;
		}
	}
?>
