<?php
	class password_model extends Banshee\model {
		public function get_container($container_id) {
			return $this->borrow("container")->get_container($container_id);
		}

		public function get_crumbs($container_id) {
			return $this->borrow("container")->get_crumbs($container_id);
		}

		public function get_password($password_id) {
			$query = "select p.* from passwords p, containers c ".
			         "where p.id=%d and p.container_id=c.id and c.user_id=%d";
			if (($result = $this->db->execute($query, $password_id, $this->user->id)) == false) {
				return false;
			}
			$password = $result[0];

			$rsa = new \Banshee\Protocols\RSA($_COOKIE["private_key"].$_SESSION["private_key"]);
			$password["crypto_key"] = $rsa->decrypt_with_private_key($password["crypto_key"]);

			$aes = new Banshee\Protocols\AES256($password["crypto_key"]);
			$password["password"] = $aes->decrypt($password["password"]);
			$password["info"] = $aes->decrypt($password["info"]);

			return $password;
		}

		public function get_all_containers() {
			return $this->borrow("container")->get_all_containers(null);
		}

		public function save_oke($password) {
			$result = true;

			if (isset($password["id"])) {
				if ($this->get_password($password["id"]) == false) {
					return false;
				}
			}

			if ($this->borrow("container")->valid_container_id($password["container_id"]) == false) {
				$this->view->add_message("Invalid container id.");
				$result = false;
			} else if ($password["container_id"] == 0) {
				$this->view->add_message("Invalid container id.");
				$result = false;
			}

			$fields = array("name");
			if (isset($password["id"]) == false) {
				array_push($fields, "password");
			}
			foreach ($fields as $field) {
				if (trim($password[$field]) == "") {
					$this->view->add_message("Fill in the ".$field.".");
					$result = false;
				}
			}

			return $result;
		}

		public function create_password($password) {
			$keys = array("id", "container_id", "crypto_key", "name", "url", "username", "password", "info");

			$password["id"] = null;
			$password["crypto_key"] = random_string(32);

			$aes = new \Banshee\Protocols\AES256($password["crypto_key"]);
			$password["password"] = $aes->encrypt($password["password"]);
			$password["info"] = $aes->encrypt($password["info"]);

			$rsa = new \Banshee\Protocols\RSA($_COOKIE["private_key"].$_SESSION["private_key"]);
			if (($password["crypto_key"] = $rsa->encrypt_with_public_key($password["crypto_key"])) == false) {
				return false;
			}

			return $this->db->insert("passwords", $password, $keys);
		}

		public function update_password($password) {
			$keys = array("container_id", "name", "url", "username", "info");

			if (($current = $this->get_password($password["id"])) == false) {
				return false;
			}

			$aes = new \Banshee\Protocols\AES256($current["crypto_key"]);
			if ($password["password"] != "") {
				array_push($keys, "password");
				$password["password"] = $aes->encrypt($password["password"]);
			}
			$password["info"] = $aes->encrypt($password["info"]);

			return $this->db->update("passwords", $password["id"], $password, $keys);
		}

		public function delete_oke($password) {
			return $this->get_password($password["id"]) !== false;
		}

		public function delete_password($password_id) {
			$queries = array(
				array("delete from shared where password_id=%d", $password_id),
				array("delete from passwords where id=%d", $password_id));

			return $this->db->transaction($queries);
		}

		public function get_users() {
			$query = "select id, fullname from users where id!=%d order by fullname";

			return $this->db->execute($query, $this->user->id);
		}

		public function get_shares($password_id) {
			$query = "select s.id, u.fullname, s.access, s.user_id from shared s, users u ".
			         "where s.user_id=u.id and s.password_id=%d";

			if (($shares = $this->db->execute($query, $password_id)) === false) {
				return false;
			}

			$result = array();
			foreach ($shares as $share) {
				$result[$share["user_id"]] = $share;
			}

			return $result;
		}

		public function save_share($share) {
			if (($password = $this->get_password($share["password_id"])) == false) {
				return false;
			}

			$this->db->query("begin");

			$query = "delete from shared where password_id=%d";
			if ($this->db->query($query, $share["password_id"]) === false) {
				$this->db->query("rollback");
				return false;
			}

			if (is_array($share["access"])) {
				foreach ($share["access"] as $user_id => $access) {
					if ($access == SHARE_ACCESS_NONE) {
						continue;
					}

					if ($user_id == $this->user->id) {
						continue;
					}

					if (($user = $this->db->entry("users", $user_id)) === false) {
						$this->db->query("rollback");
						return false;
					}

					$rsa = new \Banshee\Protocols\RSA($user["public_key"]);
					$crypto_key = $rsa->encrypt_with_public_key($password["crypto_key"]);

					$data = array(
						"id"           => null,
						"user_id"      => $user_id,
						"password_id"  => $share["password_id"],
						"crypto_key"   => $crypto_key,
						"access"       => $access);

					if ($this->db->insert("shared", $data) === false) {
						$this->db->query("rollback");
						return false;
					}
				}
			}

			return $this->db->query("commit") !== false;
		}
	}
?>
