<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * This file is part of the Banshee PHP framework
	 * https://www.banshee-php.org/
	 *
	 * Licensed under The MIT License
	 */

	namespace Banshee\Core;

	final class user {
		private $db = null;
		private $settings = null;
		private $session = null;
		private $record = array();
		private $logged_in = false;
		private $is_admin = false;

		/* Constructor
		 *
		 * INPUT:  object database, object settings, object session
		 * OUTPUT: -
		 * ERROR:  -
		 */
		public function __construct($db, $settings, $session) {
			$this->db = $db;
			$this->settings = $settings;
			$this->session = $session;

			/* Basic HTTP Authentication for web services
			 */
			if (isset($_SERVER["HTTP_AUTHORIZATION"])) {
				list($method, $auth) = explode(" ", $_SERVER["HTTP_AUTHORIZATION"], 2);
				if (($method == "Basic") && (($auth = base64_decode($auth)) !== false)) {
					list($username, $password) = explode(":", $auth, 2);
					if ($this->login_password($username, $password) == false) {
						header("Status: 401");
					} else {
						$this->session->bind_to_ip();
					}
				}
			}

			if ($this->session->user_id !== null) {
				$this->load_user_record($this->session->user_id);

				$aes = new \Banshee\Protocols\AES256($this->settings->secret_website_code);
				$_COOKIE["private_key"] = $aes->decrypt($_COOKIE["private_key"]);
			}
		}

		/* Magic method get
		 *
		 * INPUT:  string key
		 * OUTPUT: mixed value
		 * ERROR:  null
		 */
		public function __get($key) {
			switch ($key) {
				case "logged_in": return $this->logged_in;
				case "is_admin": return $this->is_admin;
				case "do_not_track": return $_SERVER["HTTP_DNT"] == 1;
				case "session": return $this->session;
				default:
					if (isset($this->record[$key])) {
						return $this->record[$key];
					}
			}

			return null;
		}

		/* Store user information from database in $this->record
		 *
		 * INPUT:  int user identifier
		 * OUTPUT: -
		 * ERROR:  -
		 */
		private function load_user_record($user_id) {
			if (($this->record = $this->db->entry("users", $user_id)) == false) {
				$this->logout();
			} else if ($this->record["status"] == USER_STATUS_DISABLED) {
				$this->logout();
			} else {
				$this->logged_in = true;

				$this->record["role_ids"] = array();
				$query = "select role_id from user_role where user_id=%d";
				if (($roles = $this->db->execute($query, $this->record["id"])) != false) {
					foreach ($roles as $role) {
						array_push($this->record["role_ids"], $role["role_id"]);
						if ((int)$role["role_id"] === (int)ADMIN_ROLE_ID) {
							$this->is_admin = true;
						}
					}
				}
			}
		}

		/* Login user
		 *
		 * INPUT:  int user id
		 * OUTPUT: -
		 * ERROR:  -
		 */
		private function login($user_id) {
			$this->session->set_user_id($user_id);
			$this->load_user_record($user_id);
			$this->log_action("user logged-in");
		}

		/* Verify user credentials
		 *
		 * INPUT:  string username, string password[, string authenticator code]
		 * OUTPUT: boolean login correct
		 * ERROR:  -
		 */
		public function login_password($username, $password, $code = null) {
			$query = "select * from users where username=%s and status!=%d limit 1";
			if (($data = $this->db->execute($query, $username, USER_STATUS_DISABLED)) == false) {
				header("X-Hiawatha-Monitor: failed_login");
				sleep(1);
				return false;
			}
			$user = $data[0];

			if (is_false(USE_AUTHENTICATOR)) {
				$auth_code_ok = true;
			} else if ($user["authenticator_secret"] === null) {
				$auth_code_ok = true;
			} else {
				$authenticator = new \Banshee\authenticator;
				$auth_code_ok = $authenticator->verify_code($user["authenticator_secret"], $code);
			}

			if (strlen($password) <= PASSWORD_MAX_LENGTH) {
				if (password_verify($password, $user["password"]) && $auth_code_ok) {
					$this->login((int)$user["id"]);

					$aes = new \Banshee\Protocols\AES256($_POST["password"]);
					$private_key = $aes->decrypt($user["private_key"]);
					unset($aes);

					$_COOKIE["private_key"] = substr($private_key, 0, 1024);
					$_SESSION["private_key"] = substr($private_key, 1024);

					$aes = new \Banshee\Protocols\AES256($this->settings->secret_website_code);
					setcookie("private_key", $aes->encrypt($_COOKIE["private_key"]));
					unset($aes);
				}
			}

			if ($this->logged_in == false) {
				header("X-Hiawatha-Monitor: failed_login");
				sleep(1);
			}

			return $this->logged_in;
		}

		/* Logout current user
		 *
		 * INPUT:  -
		 * OUTPUT: -
		 * ERROR:  -
		 */
		public function logout() {
			unset($_COOKIE["private_key"]);
			setcookie("private_key", "", 1);

			$this->log_action("user logged-out");

			$this->session->reset();

			$this->logged_in = false;
			$this->record = array();
			$this->is_admin = false;
		}

		/* Checks if user has access to page
		 *
		 * INPUT:  string page identifier
		 * OUTPUT: boolean user has access to page
		 * ERROR:  -
		 */
		public function access_allowed($page) {
			static $access = array();

			/* Always access
			 */
			$allowed = array(LOGOUT_MODULE);
			if ($this->is_admin || in_array($page, $allowed)) {
				return true;
			}

			/* Public module
			 */
			if (in_array($page, page_to_module(config_file("public_modules")))) {
				return true;
			}

			/* Public page in database
			*/
			$query = "select count(*) as count from pages where url=%s and private=%d";
			if (($result = $this->db->execute($query, "/".$page, NO)) == false) {
				return false;
			} else if ($result[0]["count"] > 0) {
				return true;
			}

			/* No roles, no access
			 */
			if (is_array($this->record["role_ids"]) == false) {
				return false;
			} else if (count($this->record["role_ids"]) == 0) {
				return false;
			}

			/* Cached?
			 */
			if (isset($access[$page])) {
				return $access[$page];
			}

			/* Check access
			 */
			$conditions = $rids = array();
			foreach ($this->record["role_ids"] as $rid) {
				array_push($conditions, "%d");
				array_push($rids, $rid);
			}

			if (in_array($page, page_to_module(config_file("private_modules")))) {
				/* Pages on disk (modules)
				 */
				$query = "select %S from roles where id in (".implode(", ", $conditions).")";
				if (($access = $this->db->execute($query, $page, $rids)) == false) {
					return false;
				}
			} else {
				/* Pages in database
				 */
				$query = "select a.level from page_access a, pages p ".
				         "where a.page_id=p.id and p.url=%s and a.level>0 ".
				         "and a.role_id in (".implode(", ", $conditions).")";
				if (($access = $this->db->execute($query, "/".$page, $rids)) == false) {
					return false;
				}
			}

			$access[$page] = max(array_flatten($access)) > 0;

			return $access[$page];
		}

		/* Verify if user has a certain role
		 *
		 * INPUT:  int role identifier / string role name
		 * OUTPUT: boolean user has role
		 * ERROR:  -
		 */
		public function has_role($role) {
			if (is_int($role)) {
				return in_array($role, $this->record["role_ids"]);
			} else if (is_string($role)) {
				if (($entry = $this->db->entry("roles", $role, "name")) != false) {
					return $this->has_role((int)$entry["id"]);
				}
			} else if (is_array($role)) {
				foreach ($role as $item) {
					if ($this->has_role($item)) {
						return true;
					}
				}
			}

			return false;
		}

		/* Log user action
		 *
		 * INPUT:  string action / format[, mixed arg, ...]
		 * OUTPUT: -
		 * ERROR:  -
		 */
		public function log_action() {
			static $logfile = null;

			if ($logfile === null) {
				$logfile = new \Banshee\logfile("actions");
			}

			if ($this->logged_in == false) {
				$logfile->user_id = "-";
			} else if (isset($_SESSION["user_switch"]) == false) {
				$logfile->user_id = $this->id;
			} else {
				$logfile->user_id = $_SESSION["user_switch"].":".$this->id;
			}

			$arguments = func_get_args();
			call_user_func_array(array($logfile, "add_entry"), $arguments);
		}
	}
?>
