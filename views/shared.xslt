<?xml version="1.0" ?>
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="banshee/main.xslt" />

<!--
//
//  Overview template
//
//-->
<xsl:template match="overview">
<table class="table table-striped table-condensed">
<thead>
<tr><th>Name</th></tr>
</thead>
<tbody>
<xsl:for-each select="password">
<xsl:if test="fullname">
<tr><td class="owner">Shared by <xsl:value-of select="fullname" /></td></tr>
</xsl:if>
<tr class="click file" onClick="javascript:document.location='/{/output/page}/{@id}'">
<td><xsl:value-of select="name" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>
</xsl:template>

<!--
//
//  Password template
//
//-->
<xsl:template match="password">
<table class="table table-striped table-condensed password">
<tr><td>Name:</td><td><xsl:value-of select="name" /></td></tr>
<xsl:if test="url!=''">
<tr><td>URL:</td><td><a href="{url}"><xsl:value-of select="url" /></a></td></tr>
</xsl:if>
<xsl:if test="username!=''">
<tr><td>Username:</td><td><xsl:value-of select="username" /></td></tr>
</xsl:if>
<tr><td>Password:</td><td><input id="password" readonly="readonly" class="text"><xsl:attribute name="onClick">this.select()</xsl:attribute></input></td></tr>
<tr id="inforow"><td>Information:</td><td><span id="info"></span></td></tr>
</table>

<div class="btn-group">
<a href="/{/output/page}" class="btn btn-default">Back</a>
</div>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<h1>Shared passwords</h1>
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="password" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
