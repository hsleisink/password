<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:include href="banshee/main.xslt" />

<!--
//
//  Search template
//
//-->
<xsl:template name="search">
<div class="search">
<form action="/{/output/page}" method="post">
<input type="hidden" name="container_id" value="{overview/@id}" />
<input type="text" id="search" name="search" placeholder="Search" class="form-control" />
<input type="hidden" name="submit_button" value="Search" />
</form>
</div>
</xsl:template>

<!--
//
//  Overview template
//
//-->
<xsl:template match="overview">
<table class="table table-striped table-condensed">
<thead>
<tr><th>Name</th><th></th></tr>
</thead>
<tbody>
<xsl:if test="@id!=0">
<tr class="click dir" onClick="javascript:document.location='/{/output/page}/{@parent_id}'">
<td>..</td>
<td></td>
</tr>
</xsl:if>
<xsl:for-each select="container">
<tr class="click dir" onClick="javascript:document.location='/{/output/page}/{@id}'">
<td><xsl:value-of select="name" /></td>
<td></td>
</tr>
</xsl:for-each>
<xsl:for-each select="password">
<tr class="click file"><xsl:attribute name="onClick">javascript:document.location='/<xsl:choose><xsl:when test="container_id">password</xsl:when><xsl:otherwise>shared</xsl:otherwise></xsl:choose>/<xsl:value-of select="@id" />'</xsl:attribute>
<td><span><xsl:value-of select="path" /></span> <xsl:value-of select="name" /></td>
<td><xsl:if test="shared>0">*</xsl:if></td>
</tr>
</xsl:for-each>
</tbody>
</table>
<xsl:apply-templates select="pagination" />

<xsl:if test="@id">
<div class="btn-group">
<xsl:if test="@id!=0">
<a href="/password/{@id}/new" class="btn btn-default">New password</a>
</xsl:if>
<a href="/{/output/page}/{@id}/new" class="btn btn-default">New container</a>
<xsl:if test="@id>0">
<a href="/{/output/page}/{@id}/edit" class="btn btn-default">Edit container</a>
</xsl:if>
</div>
</xsl:if>
</xsl:template>

<!--
//
//  Edit template
//
//-->
<xsl:template match="edit">
<xsl:call-template name="show_messages" />
<form action="/{/output/page}/{container/parent_id}" method="post">
<xsl:if test="container/@id">
<input type="hidden" name="id" value="{container/@id}" />
</xsl:if>
<xsl:if test="not(container/@id)">
<input type="hidden" name="parent_id" value="{container/parent_id}" />
</xsl:if>

<table class="edit">
<label for="name">Container:</label>
<input type="text" id="name" name="name" value="{container/name}" class="form-control" autofocus="autofocus" />
<xsl:if test="container/@id">
<label for="parent">Parent container:</label>
<select id="parent" name="parent_id" class="form-control">
<xsl:for-each select="containers/container">
<option value="{@id}">
<xsl:if test="@id=../../container/parent_id"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
<xsl:value-of select="." />
</option>
</xsl:for-each>
</select>
</xsl:if>
</table>

<div class="btn-group">
<input type="submit" name="submit_button" value="Save container" class="btn btn-default" />
<xsl:choose><xsl:when test="container/@id">
<a href="/{/output/page}/{container/@id}" class="btn btn-default">Cancel</a>
</xsl:when>
<xsl:otherwise>
<a href="/{/output/page}/{container/parent_id}" class="btn btn-default">Cancel</a>
</xsl:otherwise></xsl:choose>
<xsl:if test="container/@id">
<input type="submit" name="submit_button" value="Delete container" class="btn btn-default" onClick="javascript:return confirm('DELETE: Are you sure?')" />
</xsl:if>
</div>
</form>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<h1>Passwords</h1>
<xsl:call-template name="search" />
<xsl:apply-templates select="crumbs" />
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="edit" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
